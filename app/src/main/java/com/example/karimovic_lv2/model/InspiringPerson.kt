package com.example.karimovic_lv2.model

data class InspiringPerson(
    var id: Long? = null,
    var name: String,
    val details: String) {

}