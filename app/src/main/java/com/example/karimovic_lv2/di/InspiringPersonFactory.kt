package com.example.karimovic_lv2.di

import com.example.karimovic_lv2.data.InspiringPersonRepository
import com.example.karimovic_lv2.data.InspiringPersonRepositoryImpl
import com.example.karimovic_lv2.data.memory_db.InMemoryDb

object InspiringPersonFactory {
    private val  inspiringPersonDao = InMemoryDb()
    val inspiringPersonRepository: InspiringPersonRepository = InspiringPersonRepositoryImpl(
        inspiringPersonDao)

}