package com.example.karimovic_lv2.data

import com.example.karimovic_lv2.model.InspiringPerson

class InspiringPersonRepositoryImpl(val inspiringPersonDao: InspiringPersonDao) : InspiringPersonRepository {
    override fun save(inspiringPerson: InspiringPerson) = inspiringPersonDao.save(inspiringPerson)
    override fun delete(inspiringPerson: InspiringPerson) = inspiringPersonDao.delete(inspiringPerson)
    override fun getInspiringPersonById(id: Long): InspiringPerson? = inspiringPersonDao.getInspiringPersonById(id)
    override fun getAllInspiringPersons(): List<InspiringPerson> = inspiringPersonDao.getAllInspiringPersons()

}