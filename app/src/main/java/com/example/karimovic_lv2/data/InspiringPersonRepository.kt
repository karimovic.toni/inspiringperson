package com.example.karimovic_lv2.data

import com.example.karimovic_lv2.model.InspiringPerson

interface InspiringPersonRepository {
    fun save(inspiringPerson: InspiringPerson)
    fun delete(inspiringPerson: InspiringPerson)
    fun getInspiringPersonById(id: Long): InspiringPerson?
    fun getAllInspiringPersons(): List<InspiringPerson>
}