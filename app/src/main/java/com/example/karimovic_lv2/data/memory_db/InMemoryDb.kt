package com.example.karimovic_lv2.data.memory_db

import android.content.Context
import android.content.res.Resources
import androidx.core.content.ContextCompat
import com.example.karimovic_lv2.R
import com.example.karimovic_lv2.data.InspiringPersonDao
import com.example.karimovic_lv2.model.InspiringPerson




class InMemoryDb : InspiringPersonDao{
    private val inspiringPersons = mutableListOf<InspiringPerson>()

    companion object{
        private const val LINUS_TORVALDS_NAME : String = "Linus Torvalds"
        private const val LINUS_TORVALDS_DETAILS : String = "Linus Benedict Torvalds, finski znanstvenik, kreator je Linux kernela. Iako je studirao računarstvo, fakultet nikada nije završio. Linus je, inspiriran Minix-om, kojeg je napravio Andrew Tanenbaum, napravio operacijski sustav sličan Unixu, kojem je dao ime Linux."
        private const val MARK_ZUCKERBERG_NAME : String ="Mark Zuckerburg"
        private const val MARK_ZUCKERBERG_DETAILS : String ="Mark Elliot Zuckerberg je američki računalni programer, vlasnik-direktor i glavni kreator najveće socijalno društvene internetske stranice Facebooka koja je osnovana kao privatno poduzeće 2004. godine."
        private const val STEVE_JOBS_NAME : String ="Steve Jobs"
        private const val STEVE_JOBS_DETAILS : String = "Steve Jobs je bio izvršni direktor, biznismen i dizajner tvrtke Apple Inc., čiji je suosnivač. Bio je jedan je od najutjecajnijih ljudi u svijetu računalne industrije. Neki od poznatijih proizvoda tvrke Apple u čijem je razvijanju sudjelovao su iPhone, iPad, iPod i Mac."
    }



    init {
            save(InspiringPerson(0, LINUS_TORVALDS_NAME, LINUS_TORVALDS_DETAILS))
            save(InspiringPerson(1, MARK_ZUCKERBERG_NAME , MARK_ZUCKERBERG_DETAILS ))
            save(InspiringPerson(2, STEVE_JOBS_NAME, STEVE_JOBS_DETAILS))
    }

    override fun save(inspiringPerson: InspiringPerson) {
        inspiringPersons.add(inspiringPerson)
    }

    override fun delete(inspiringPerson: InspiringPerson) {
        inspiringPersons.remove(inspiringPerson)
    }

    override fun getInspiringPersonById(id: Long): InspiringPerson? {
        return inspiringPersons.firstOrNull { it.id == id }
    }

    override fun getAllInspiringPersons(): List<InspiringPerson> {
        return  inspiringPersons
    }

}