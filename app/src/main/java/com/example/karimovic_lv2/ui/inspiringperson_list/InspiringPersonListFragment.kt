package com.example.karimovic_lv2.ui.inspiringperson_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.karimovic_lv2.databinding.FragmentInspiringpersonListBinding
import com.example.karimovic_lv2.databinding.FragmentNewInspiringpersonBinding
import com.example.karimovic_lv2.di.InspiringPersonFactory

class InspiringPersonListFragment : Fragment(), OnInspiringPersonSelectedListener{

    private lateinit var binding: FragmentInspiringpersonListBinding
    private lateinit var adapter: InspiringPersonAdapter
    private val inspiringPersonRepository = InspiringPersonFactory.inspiringPersonRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInspiringpersonListBinding.inflate(layoutInflater)
        setupRecyclerView()
        binding.fabAddNote.setOnClickListener { showCreateNewInspiringPersonFragmet() }
        return binding.root
    }

    private fun setupRecyclerView() {
        binding.inspiringpersonListRvInspiringpersons.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        adapter = InspiringPersonAdapter()
        adapter.onInspiringPersonSelectedListener = this
        binding.inspiringpersonListRvInspiringpersons.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        updateData()
    }

    private fun updateData() {
        adapter.setInspiringPerson(inspiringPersonRepository.getAllInspiringPersons())
    }

    companion object {
        val Tag = "Inspiring Persons List"

        fun create(): Fragment {
            return InspiringPersonListFragment()
        }
    }

    override fun onInspiringPersonSelected(id: Long?) {
        val action =
            InspiringPersonListFragmentDirections.actionInspiringPersonListFragmentToInspiringPersonDetailsFragment(id ?: -1)
        findNavController().navigate(action)
    }

    private fun showCreateNewInspiringPersonFragmet() {
        val action =
            InspiringPersonListFragmentDirections.actionInspiringPersonListFragmentToNewInspiringPersonFragment()
        findNavController().navigate(action)
    }

}