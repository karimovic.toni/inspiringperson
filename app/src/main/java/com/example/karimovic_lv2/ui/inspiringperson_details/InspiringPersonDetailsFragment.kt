package com.example.karimovic_lv2.ui.inspiringperson_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.karimovic_lv2.databinding.FragmentInspiringpersonDetailsBinding
import com.example.karimovic_lv2.databinding.FragmentInspiringpersonListBinding
import com.example.karimovic_lv2.di.InspiringPersonFactory
import com.example.karimovic_lv2.model.InspiringPerson

class InspiringPersonDetailsFragment : Fragment (){

    private lateinit var binding: FragmentInspiringpersonDetailsBinding
    private val inspiringPersonRepository = InspiringPersonFactory.inspiringPersonRepository
    private val args: InspiringPersonDetailsFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInspiringpersonDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val inspiringPerson = inspiringPersonRepository.getInspiringPersonById(args.inspiringPersonId)
        display(inspiringPerson)
    }

    private fun display(inspiringPerson: InspiringPerson?) {
        inspiringPerson?.let {
            binding.apply {
                tvInspiringpersonDetailsTitle.text = inspiringPerson.name
                tvTaskDetailsContents.text = inspiringPerson.details
            }
        }
    }

    companion object {
        val Tag = "InspiringPersonDetails"
        val TaskIdKey = "TaskId"

        fun create(id: Long): Fragment {
            val fragment = InspiringPersonDetailsFragment()
            return fragment
        }
    }


}