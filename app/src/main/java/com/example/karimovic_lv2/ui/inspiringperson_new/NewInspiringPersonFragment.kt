package com.example.karimovic_lv2.ui.inspiringperson_new

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.karimovic_lv2.R
import com.example.karimovic_lv2.databinding.FragmentNewInspiringpersonBinding
import com.example.karimovic_lv2.di.InspiringPersonFactory
import com.example.karimovic_lv2.model.InspiringPerson

class NewInspiringPersonFragment : Fragment() {

    lateinit var binding: FragmentNewInspiringpersonBinding
    private  val inspiringPersonRepository = InspiringPersonFactory.inspiringPersonRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewInspiringpersonBinding.inflate(layoutInflater)
        binding.bSaveTask.setOnClickListener { saveTask() }
        return binding.root
    }

    private fun saveTask() {

        val name = binding.etInspiringpersonNameInput.text.toString()
        val details = binding.etInspiringpersonDetailsInput.text.toString()
        val id = inspiringPersonRepository.getAllInspiringPersons().size
        inspiringPersonRepository.save(InspiringPerson(id.toLong(), name, details))
        Toast.makeText(context, getString(R.string.message_saving), Toast.LENGTH_SHORT).show()

        val action = NewInspiringPersonFragmentDirections.actionNewInspiringPersonFragmentToInspiringPersonListFragment()
        findNavController().navigate(action)

    }

    companion object {
        val Tag = "NewInspiringPersonFragment"

        fun create(): Fragment {
            return NewInspiringPersonFragment()
        }
    }

}