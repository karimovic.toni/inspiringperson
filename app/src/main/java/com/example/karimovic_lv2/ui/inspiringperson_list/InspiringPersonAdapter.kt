package com.example.karimovic_lv2.ui.inspiringperson_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.karimovic_lv2.R
import com.example.karimovic_lv2.databinding.ItemInspiringpersonBinding
import com.example.karimovic_lv2.model.InspiringPerson

class InspiringPersonAdapter : RecyclerView.Adapter<InspiringPersonViewHolder> () {

    val inspiringPersons = mutableListOf<InspiringPerson>()
    var onInspiringPersonSelectedListener: OnInspiringPersonSelectedListener? = null

    fun setInspiringPerson(inspirinPersons: List<InspiringPerson>){
        this.inspiringPersons.clear()
        this.inspiringPersons.addAll(inspirinPersons)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InspiringPersonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_inspiringperson, parent, false)
        return InspiringPersonViewHolder(view)
    }

    override fun onBindViewHolder(holder: InspiringPersonViewHolder, position: Int) {
        val inspiringPerson = inspiringPersons[position]
        holder.bind(inspiringPerson)
        onInspiringPersonSelectedListener?.let { listener ->
            holder.itemView.setOnClickListener { listener.onInspiringPersonSelected(inspiringPerson.id) }
        }

    }

    override fun getItemCount(): Int = inspiringPersons.count()


}

class InspiringPersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(inspiringPerson: InspiringPerson) {
        val binding = ItemInspiringpersonBinding.bind(itemView)
        binding.itemInspiringpersonName.text = inspiringPerson.name
        //binding.itemInspiringpersonDetails.text = inspiringPerson.details
    }
}