package com.example.karimovic_lv2.ui.inspiringperson_list

interface OnInspiringPersonSelectedListener {
    fun onInspiringPersonSelected(id: Long?)
}